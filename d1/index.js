//Basic imports
const express = require("express");
const mongoose = require("mongoose");

//allows us to use contents of "taskRoutes.js" in the "routes" folder

const taskRoutes = require("./routes/taskRoutes.js");

//Server setup
const app = express();
const port = 3000;

//MongoDB Connection
mongoose.connect("mongodb+srv://aronsc26:diwa1234@wdc028-course-booking.sysnpgd.mongodb.net/b190-to-do?retryWrites=true&w=majority",
	
	{
		useNewUrlParser: true,
		useUnifiedTopology: true
	}
	);

let db = mongoose.connection;

db.on("error", console.error.bind(console,"connection error"));
db.once("open", () => console.log("we're connected to the database"));

app.use(express.json());
app.use(express.urlencoded({extended:true}));

//allows all the tasks routes created in the taskRoutes.js files to use "/tasks" route
app.use("/tasks", taskRoutes);

//Server Running Confirmation

app.listen(port,() => console.log(`Server Running at port ${port}`));