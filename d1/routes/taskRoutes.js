
//constains all endpoints for our application
//app.js/index.js should only be concerned with information about the server
//we have to use the Router method inside express

const express = require("express");

//allows us to access HTTP method middlewares that makes it easier to create routing system
const router = express.Router();

//this allows us to use the contents of "taskController.js" in "controllers" folder
const taskController = require("..//controllers/taskController.js");

//route to get all tasks
/*
	get request to be sent at localhost:3000/tasks
*/
router.get("/",(req,res)=>{
	//since the controller did not send any response, the task falls to the routes to send a response that is received from the controllers and send it to the frontend
	taskController.getAllTasks().then(resultFromController => res.send(resultFromController));
});

//mini activity
//create "/" route that will prcess the request and send the response based on the createTask function inside the "taskController.js"

router.post("/",(req,res)=>{
	taskController.createTask(req.body).then(resultFromController => res.send(resultFromController));
});

//route for deleting a task
router.delete("/:id",(req,res)=>{
	taskController.deleteTask(req.params.id).then(resultFromController => res.send(resultFromController));
});

//route for updating a task
router.put("/:id",(req,res)=>{
	taskController.updateTask(req.params.id, req.body).then(resultFromController => res.send(resultFromController));
});



/*
	npx kill-port <port> -
*/



//SECTION - ACTIVITY


//route to find specific task
router.get("/:id",(req,res)=>{
	taskController.getSpecificTask(req.params.id).then(resultFromController => res.send(resultFromController));
});

//route id to update status


router.put("/:id/complete",(req,res)=>{
	taskController.updateStatus(req.params.id, req.body).then(resultFromController => res.send(resultFromController));
});


//module.exports = allows us to export the file where it is inserted to be used by other files such as app.js/index.js
module.exports= router;


