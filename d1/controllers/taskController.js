//contains function and business logic of our express js application
//all operations it can do will be placed in this file.

const Task = require("../models/task.js");

//defines the functions to be used in the "taskRoutes.js"
module.exports.getAllTasks = () => {
	return Task.find({}).then(result => {
		return result;
	});
};


//function for creating a task

module.exports.createTask = (requestBody) => {
	let newTask = new Task({
		name: requestBody.name
	});
	//waits for the save method to complete before detecting any errors
	return newTask.save().then((task,error) => {
		if(error){
			console.log(error);
			return false;
		}else{
			return task;
		};
	});
};

//mini activity for delete task
module.exports.deleteTask = (taskId) => {
	// findByIdAndRemove - is a mongoose method that searches for the documents using the _id properties; after finding a document, the job of this method is to remove the document
	return Task.findByIdAndRemove(taskId).then((result, error) => {
		if(error){
			console.log(error);
			return false;
		}else{
			return result
		};
	});
};

//function for updating task
module.exports.updateTask = (taskId, newContent) => {
	return Task.findById(taskId).then((result, error) => {
		if(error){
			console.log(error);
			return false;
		}else{
			result.name = newContent.name;
			return result.save().then((updatedTask, error) =>{
				if(error){
					console.log(error)
					return false;
				}else{
					return updatedTask
				};
			});
		};
	});
};


//SECTION - ACTIVITY

 	module.exports.getSpecificTask = (taskId) => {
 		return Task.findById(taskId).then((result, error) => {
 			if(error){
 				console.log(error);
 				return false;
 			}else{
 				return result;
 			};
 		});
 	};


 	module.exports.updateStatus = (taskId, newContent) => {
	return Task.findById(taskId).then((result, error) => {
		if(error){
			console.log(error);
			return false;
		}else{
			result.status= "complete";
			return result.save().then((updatedStatus, error) =>{
				if(error){
					console.log(error)
					return false;
				}else{
					return updatedStatus;
				};
			});
		};
	});
};