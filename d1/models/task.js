//model files should import the mongoose module

const mongoose = require("mongoose");


const taskSchema = new mongoose.Schema({
	name: String,
	status: {
		type: String,
		default: "pending"
	}
});

//module.exports = allows us to export the file where it is inserted to be used by other files such as app.js/index.js
module.exports = mongoose.model("Task",taskSchema);